Introduction
=======================

**Logging service engine extension**

This logging extension plugin for Eclipse Data Connector captures live events (ASSET EVENT, POLICY EVENT, or CONTRACT EVENT) and sends them to the Logging API at DataMite API.

