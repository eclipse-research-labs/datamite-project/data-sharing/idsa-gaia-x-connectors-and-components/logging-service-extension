Installation and Requirements
===========================


**Prerequisites**

Java Development Kit (JDK): Ensure you have JDK 17 or later installed.

**Work with**


Create a new gradle project and add that code in it. In the future, the idea is to create a library and add the dependency to it, like this:

```java
dependencies {
    implementation("com.datamite.edc.extension:core-events:$version")
}