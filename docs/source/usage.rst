Usage
======

Configure the Logging API endpoint:
Ensure the Logging API endpoint is correctly configured in your application properties.

**Run the application**




```sh
gradle run


**Programming Languages**



Java: The main programming language used for this extension.
Gradle: Build automation tool used for managing dependencies and build tasks.