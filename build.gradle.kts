import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    `java-library`
    id("com.github.johnrengelman.shadow") version "8.1.1"
    id("java")
    `maven-publish`
}

group = "com.tecnalia.hpa.edc.extension"
version = "0.10.1-0.0.1-SNAPSHOT"

val edc_version = "0.10.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.eclipse.edc:asset-spi:$edc_version")
    implementation("org.eclipse.edc:contract-spi:$edc_version")
    implementation("org.eclipse.edc:policy-spi:$edc_version")
    implementation("org.eclipse.edc:control-plane-core:$edc_version")
    implementation("org.eclipse.edc:control-plane-spi:$edc_version")
    implementation("org.eclipse.edc:core-spi:$edc_version")
    implementation("org.eclipse.edc:transfer-pull-http-dynamic-receiver:$edc_version")

    implementation("org.json:json:20210307")
    implementation("com.squareup.okhttp3:okhttp:4.12.0")

    // Spring for REST template
    implementation("org.springframework:spring-web:5.3.22")

    // Logging
    implementation("org.slf4j:slf4j-api:1.7.36")
    implementation("ch.qos.logback:logback-classic:1.2.11")

    // Kotlin standard library
    implementation(kotlin("stdlib"))
}

tasks.withType<ShadowJar> {
    exclude("**/pom.properties", "**/pom.xml")
    mergeServiceFiles()
    // Lo seteamos pero no vale para nada, porque se va a subir con el de la publicación
    archiveFileName.set("${rootProject.name}-${project.version}.jar")
}

publishing {
    publications {
        create<MavenPublication>(rootProject.name) {
            artifactId = rootProject.name
            artifact(tasks.shadowJar.get().archiveFile.get())
        }
    }
    repositories {
        maven {
            name = "eclipseReleases"
            url = uri("https://repo.eclipse.org/content/repositories/research-datamite-snapshots/")
            credentials {
                username = System.getenv("REPO_USERNAME") ?: ""  // Usa la variable de entorno o un valor por defecto
                password = System.getenv("REPO_PASSWORD") ?: ""
            }
        }
    }
}

