/*
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package com.tecnalia.hpa.connector.extension.audit.logging;

import org.eclipse.edc.connector.controlplane.asset.spi.event.AssetCreated;
import org.eclipse.edc.connector.controlplane.asset.spi.event.AssetDeleted;
import org.eclipse.edc.connector.controlplane.asset.spi.event.AssetUpdated;
import org.eclipse.edc.connector.controlplane.contract.spi.event.contractdefinition.ContractDefinitionCreated;
import org.eclipse.edc.connector.controlplane.contract.spi.event.contractdefinition.ContractDefinitionDeleted;
import org.eclipse.edc.connector.controlplane.contract.spi.event.contractdefinition.ContractDefinitionUpdated;
import org.eclipse.edc.connector.controlplane.policy.spi.event.PolicyDefinitionCreated;
import org.eclipse.edc.connector.controlplane.policy.spi.event.PolicyDefinitionDeleted;
import org.eclipse.edc.connector.controlplane.policy.spi.event.PolicyDefinitionUpdated;
import org.eclipse.edc.spi.event.Event;
import org.eclipse.edc.spi.event.EventEnvelope;
import org.eclipse.edc.spi.event.EventSubscriber;

import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.system.configuration.Config;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpStatus;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.FormBody;

import org.json.JSONObject;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.IOException;
import java.util.Arrays;

public class AuditLoggingSubscriber implements EventSubscriber {

    private Monitor monitor;
    private String edcHostID;
    private Config config;

    private static final String TOKEN_URL = "http://160.40.52.10:9093/realms/datamite/protocol/openid-connect/token";
    private static final String CLIENT_ID = "datamite_client_auth_test";
    private static final String USERNAME = "testUser";
    private static final String PASSWORD = "testUser1!";
    private static final String CLIENT_SECRET = "mZciObKMLv1mhurqw1DQIlKgQXrJS1Rd";
    private static final String URL_ASSET_CREATE = "https://datamite.api.iti.gr/assetLog/create";
    private static final String REQUEST_HASH_PASSWORD = "!Datamite";
    private static final OkHttpClient client = new OkHttpClient();

    public AuditLoggingSubscriber(Monitor monitor, Config conf) {
        this.monitor = monitor;
        this.config = conf;
        this.edcHostID = config.getString("edc.hostname", "localhost");
        monitor.info("EVENTS- AuditLoggingSubscriber Initialized");
    }

    private static String generateSHA256Hash(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error generando el hash SHA-256", e);
        }
    }

    private String getAuthToken() throws IOException {
        FormBody formBody = new FormBody.Builder()
                .add("client_id", CLIENT_ID)
                .add("username", USERNAME)
                .add("password", PASSWORD)
                .add("grant_type", "password")
                .add("client_secret", CLIENT_SECRET)
                .build();

        Request request = new Request.Builder()
                .url(TOKEN_URL)
                .post(formBody)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Error al obtener el token: " + response);
            }
            String responseBody = response.body().string();
            JSONObject jsonResponse = new JSONObject(responseBody);
            return jsonResponse.getString("access_token");
        }
    }

    private void sendPostRequest(String urlString, String jsonInputString) {
        try {
            monitor.info("Sending POST request: " + jsonInputString);
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; utf-8");
            conn.setRequestProperty("accept", "*/*");
            conn.setDoOutput(true);

            try (OutputStream os = conn.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            int responseCode = conn.getResponseCode();
            monitor.info("POST Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                monitor.info("POST request successful");
            } else {
                monitor.info("POST request failed with code: " + responseCode);
            }
        } catch (Exception e) {
            monitor.info("Error in POST request: " + e.getMessage());
        }
    }

    @Override
    public <E extends Event> void on(EventEnvelope<E> event) {
        String message = null;
        var payload = event.getPayload();
        monitor.info("EVENTS- Payload class: " + payload.getClass().getSimpleName());

        if (payload instanceof AssetCreated) {
            handleAssetCreated((AssetCreated) payload);
        } else if (payload instanceof AssetUpdated) {
            handleAssetUpdated((AssetUpdated) payload);
        } else if (payload instanceof AssetDeleted) {
            handleAssetDeleted((AssetDeleted) payload);
        } else if (payload instanceof PolicyDefinitionCreated) {
            handlePolicyDefinitionCreated((PolicyDefinitionCreated) payload);
        } else if (payload instanceof PolicyDefinitionUpdated) {
            handlePolicyDefinitionUpdated((PolicyDefinitionUpdated) payload);
        } else if (payload instanceof PolicyDefinitionDeleted) {
            handlePolicyDefinitionDeleted((PolicyDefinitionDeleted) payload);
        } else if (payload instanceof ContractDefinitionCreated) {
            handleContractDefinitionCreated((ContractDefinitionCreated) payload);
        } else if (payload instanceof ContractDefinitionUpdated) {
            handleContractDefinitionUpdated((ContractDefinitionUpdated) payload);
        } else if (payload instanceof ContractDefinitionDeleted) {
            handleContractDefinitionDeleted((ContractDefinitionDeleted) payload);
        } else {
            monitor.info("EVENTS- Unrecognized event: " + event.getClass().getSimpleName());
        }
    }

    private void handleAssetCreated(AssetCreated assetPayload) {
        String message = String.format("[AssetEvent] %s created asset %s", edcHostID, assetPayload.getAssetId());
        monitor.info(message);
        JSONObject json = createAssetJson(assetPayload);
        sendAssetLogRequest(json);
    }

    private void handleAssetUpdated(AssetUpdated assetPayload) {
        String message = String.format("[AssetEvent] %s updated asset %s", edcHostID, assetPayload.getAssetId());
        monitor.info(message);
    }

    private void handleAssetDeleted(AssetDeleted assetPayload) {
        String message = String.format("[AssetEvent] %s deleted asset %s", edcHostID, assetPayload.getAssetId());
        monitor.info(message);
    }

    private void handlePolicyDefinitionCreated(PolicyDefinitionCreated policyPayload) {
        String message = String.format("[PolicyDefinition] %s created policy %s", edcHostID, policyPayload.getPolicyDefinitionId());
        monitor.info(message);
    }

    private void handlePolicyDefinitionUpdated(PolicyDefinitionUpdated policyPayload) {
        String message = String.format("[PolicyDefinition] %s updated policy %s", edcHostID, policyPayload.getPolicyDefinitionId());
        monitor.info(message);
    }

    private void handlePolicyDefinitionDeleted(PolicyDefinitionDeleted policyPayload) {
        String message = String.format("[PolicyDefinition] %s deleted policy %s", edcHostID, policyPayload.getPolicyDefinitionId());
        monitor.info(message);
    }

    private void handleContractDefinitionCreated(ContractDefinitionCreated contractPayload) {
        String message = String.format("[ContractDefinition] %s created contract %s", edcHostID, contractPayload.getContractDefinitionId());
        monitor.info(message);
    }

    private void handleContractDefinitionUpdated(ContractDefinitionUpdated contractPayload) {
        String message = String.format("[ContractDefinition] %s updated contract %s", edcHostID, contractPayload.getContractDefinitionId());
        monitor.info(message);
    }

    private void handleContractDefinitionDeleted(ContractDefinitionDeleted contractPayload) {
        String message = String.format("[ContractDefinition] %s deleted contract %s", edcHostID, contractPayload.getContractDefinitionId());
        monitor.info(message);
    }

    private JSONObject createAssetJson(AssetCreated assetPayload) {
        JSONObject json = new JSONObject();
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        json.put("assetId", assetPayload.getAssetId().toString());
        json.put("assetName", assetPayload.name().toString());
        json.put("contentType", "application/json");
        json.put("version", 1);
        json.put("container", "src-container-datamite");
        json.put("blobName", "Datamite Blog Name");
        json.put("accountName", "Datamite object storage account name");
        json.put("endpointUsed", URL_ASSET_CREATE);
        json.put("ipAddress", "localhost");
        json.put("isDeleted", false);
        json.put("providerId", "bad443d1-48dd-4efe-986b-ef915cfe3294");
        json.put("providerName", "DATAMITE");
        json.put("userName", "DATAMITE");
        json.put("userId", "dc57d527-0964-4146-8c4b-e50a526759fc");
        json.put("createdAt", LocalDateTime.now().format(formatter));

        String requestHash= generateSHA256Hash(REQUEST_HASH_PASSWORD);
        json.put("requestHash", requestHash);
        return json;
    }

    private void sendAssetLogRequest(JSONObject json) {
        try {
            String authToken = getAuthToken();
            if (authToken != null) {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.set("accept", "*/*");
                headers.set("authToken", authToken);

                HttpEntity<String> entity = new HttpEntity<>(json.toString(), headers);
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> response = restTemplate.exchange(URL_ASSET_CREATE, HttpMethod.POST, entity, String.class);

                if (response.getStatusCode() == HttpStatus.OK) {
                    monitor.info("Asset log created successfully.");
                } else {
                    monitor.info("Error creating asset log: " + response.getStatusCode());
                    monitor.info(response.getBody());
                }
            } else {
                monitor.info("Unable to retrieve authToken.");
            }
        } catch (Exception e) {
            monitor.info("Error sending asset log request: " + e.getMessage());
        }
    }
}
