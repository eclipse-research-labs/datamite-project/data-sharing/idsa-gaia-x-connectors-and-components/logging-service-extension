# Logging service extension



## Overview

This logging extension plugin for Eclipse Data Connector captures live events (ASSET EVENT, POLICY EVENT, or CONTRACT EVENT) and sends them to the Logging API at DataMite API.


## Prerequisites

Java Development Kit (JDK): Ensure you have JDK 17 or later installed.

## Work with

Create a new gradle project and add that code in it. In the future the idea is to create a library and add the dependecy to it, like this:

```
dependencies {
    implementation("com.datamite.edc.extension:core-events:$version")
}
```
## Usage

Configure the Logging API endpoint:
Ensure the Logging API endpoint is correctly configured in your application properties.

### Run the application:

```
gradle run
```

## Programming Languages
Java: The main programming language used for this extension.
Gradle: Build automation tool used for managing dependencies and build tasks.


## Roadmap
<ol>
  <li>M18: Initial development and internal testing of the logging extension.</li>
  <li>M18: Public release of the library and initial feedback collection.</li>
  <li>M21: Optimization based on feedback and performance improvements.</li>
  <li>M24: Final release and documentation updates.</li>
</ol>
